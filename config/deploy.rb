# IMPORTANT:
# run cap deploy:setup before first deploy, cap deploy:check to check for issues

# this option allows capistrano to accept the key from bitbucket and put it
# in your known_hosts file

role :app, %w{esben@166.62.101.105}
role :web, %w{esben@166.62.101.105}
role :db,  %w{esben@166.62.101.105}
set :user, 'esben'
# application name (meaningless)
set :application, "jube"
set :use_sudo, false


# repository address (SSH) and type of repository. it will guess if you don't specify
# the :scm option
set :scm, :git
# set :scm_username, "Erkorsaa"
# set :scm_passphrase, ""
set :deploy_to, '/var/www/sharetribe'
set :deploy_via, :remote_cache
set :branch, "master"
set :rails_env, "production"

set :repo_url,  "git@bitbucket.org:Erkorsaa/jube.git"

# when capistrano deploys, it creates a timestamped folder and symlinks the latest;
# this specifies how many deploys to keep
set :keep_releases, 5

# directory you want the project to deploy to. cap will create a folder
# called 'current' within this directory which is a simlink to the
# latest deploy

# this is the user your local machine can log into the server as
# you must log into the server at least once so your machine has
# the remote in your known_hosts file


# this should be the remote address of the web server

# Agent forwarding can make key management much simpler as it uses
# your local keys instead of keys installed on the server. This also requires
# an entry in .ssh/config under the proper host: FowardAgent yes
set :default_run_options, {:pty => true}
#set :ssh_options, { :forward_agent => true }
set :ssh_options, {
  config: false,
  forward_agent: true
  #Other options...
}
set :port, 22

# set :linked_files, %w{ config/database.yml log tmp }

# tells capistrano what branch to use for deployments

# we want to deploy as few files as possible - this causes capistrano
# to keep a git repo of the project on the remote and uses git to pull changes

# after hook - this is what actually zaps old releases
#after "deploy:restart", "deploy:cleanup"

namespace :deploy do
  desc "Tell passenger to restart the app"
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
       execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end
  after :finishing, 'deploy:cleanup'

  # desc "Symlink shared configs and folders on each release."
  task :generate_symlink do
    on roles(:app), in: :sequence, wait: 5 do
      # run "cp -b #{shared_path}/config/database.yml #{release_path}/config"
      execute "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
      execute "ln -nfs #{shared_path}/log #{release_path}/log"
      execute "ln -nfs #{shared_path}/tmp #{release_path}/tmp"
      execute "ln -nfs #{shared_path}/public/system/images #{release_path}/public/system/images"
      execute "ln -nfs #{shared_path}/public/system/id_proofs #{release_path}/public/system/id_proofs"
      execute "ln -nfs #{shared_path}/public/system/logos #{release_path}/public/system/logos"
      execute "ln -nfs #{shared_path}/public/system/cover_photos #{release_path}/public/system/cover_photos"
      execute "ln -nfs #{shared_path}/public/system/small_cover_photos #{release_path}/public/system/small_cover_photos"
      execute "ln -nfs #{shared_path}/public/system/favicon #{release_path}/public/system/favicon"
      execute "ln -nfs #{shared_path}/public/system/wide_logos #{release_path}/public/system/wide_logos"
    end
  end

  # task :migrate do
  # end
end
after :deploy, 'deploy:cleanup'
after :deploy, 'deploy:generate_symlink'
