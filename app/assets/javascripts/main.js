$(document).on('ready', function() {
    $(".video").prop('muted', true);

    $('.to-end').on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $(".inform").offset().top},
        'slow');
    });
    $('.how_jube_works_link').on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $(".how_jube_works").offset().top},
        'slow');
    });

})

var swiper = new Swiper('.one', {
    pagination: '.swiper-pagination',
    paginationClickable: true,
    spaceBetween: 30,
    autoplay: 6000,
});


var swiperTwo = new Swiper('.two', {
    pagination: '.swiper-pagination',
    paginationClickable: true,
    spaceBetween: 30,
});

setTimeout(function() {
    var swiperTwo = new Swiper('.two', {
        autoplay: 7000,
    });
}, 3000);