window.ST = window.ST || {};

window.ST.transaction = window.ST.transaction || {};

(function(module, _) {

  function toOpResult(submitResponse) {
    if (submitResponse.op_status_url) {
      return ST.utils.baconStreamFromAjaxPolling(
        { url: submitResponse.op_status_url },
        function(pollingResult) {
          return pollingResult.completed;
        }
      ).flatMap(function (pollingResult) {
        var opResult = pollingResult.result;
        if (opResult.success) {
          return opResult;
        }
        else {
          return new Bacon.Error({ errorMsg: submitResponse.op_error_msg });
        }
      });
    } else {
      return new Bacon.Error({ errorMsg: submitResponse.error_msg });
    }
  }


  function setupSpinner($form) {
    var spinner = new Image();
    spinner.src = "https://s3.amazonaws.com/sharetribe/assets/ajax-loader-grey.gif";
    spinner.className = "paypal-button-loading-img";
    var $spinner = $(spinner);
    $form.find(".paypal-button-wrapper").append(spinner);
    $spinner.hide();

    return $spinner;
  }

  function toggleSpinner($spinner, show) {
    if (show === true) {
      $spinner.show();
    } else {
      $spinner.hide();
    }
  }


  function redirectFromOpResult(opResult) {
    window.location = opResult.data.redirect_url;
  }

  function showErrorFromOpResult(opResult) {
    ST.utils.showError(opResult.errorMsg, "error");
  }


  function initializePayPalBuyForm(formId) {
    var $form = $('#' + formId);
    var formAction = $form.attr('action');
    var $spinner = setupSpinner($form);

    // EventStream of true/false
    var submitInProgress = new Bacon.Bus();

    var formSubmitWithData = $form.asEventStream('submit', function(ev) {
      ev.preventDefault();
      return $form.serialize();
    })
      .filter(submitInProgress.not().toProperty(true)); // Prevent concurrent submissions

    var opResult = formSubmitWithData
      .flatMapLatest(function (data) { return Bacon.$.ajaxPost(formAction, data); })
      .flatMapLatest(toOpResult);

    submitInProgress.plug(formSubmitWithData.map(true));
    // Success response to operation keeps submissions blocked, error releases
    submitInProgress.plug(opResult.map(true).mapError(false));
    submitInProgress.skipDuplicates().onValue(_.partial(toggleSpinner, $spinner));

    opResult.onValue(redirectFromOpResult);
    opResult.onError(showErrorFromOpResult);
  }

  function initializeCreatePaymentPoller(opStatusUrl, redirectUrl) {
    ST.utils.baconStreamFromAjaxPolling(
      { url: opStatusUrl },
      function(pollingResult) {
        return pollingResult.completed;
      }
    ).onValue(function () { window.location = redirectUrl; });
  }

  function initializeFreeTransactionForm(locale) {
    window.auto_resize_text_areas("text_area");
    $('textarea').focus();
    var form_id = "#transaction-form";
    $(form_id).validate({
      rules: {
        "message": {required: true}
      },
      submitHandler: function(form) {
        window.disable_and_submit(form_id, form, "false", locale);
      }
  });

  }

  $(".notification_status").on('click', function(){
    var id = $(this).attr("id");
    var person_id = $(this).attr("person_id");
    var locale = $(this).attr("locale");
    var value = $(this).val();
    var url = "/"+ locale+"/"+person_id+"/transactions/"+id+"/notification_status"
    $(".card_small_loader").show();
    $.ajax({
      url: url,
      data: {notification_status: value},
      type: 'post',
      dataType: 'json',
      success: function (response) {
        if(response["notification_status"] == "accept"){
          $(".notify").html("<font style='color:green;'> Successfully accepted, now "+response["sender"]+" will able to buy "+response["title"] +"!!  </font>" )
        }else
        if(response["notification_status"] == "decline"){
          $(".notify").html("<font style='color:green;'> Successfully declined!! </font>" )
        }
        $(".card_small_loader").hide();
        location.reload(); 
      }
    });
  })

  $("#stripe_detail_country").on('change', function(){
    var value = $(this).find('option:selected').text()
    var currency_url = $("#currency_url").val();
    if(value != "Select One")
    {
      $.ajax({
        url: currency_url,
        data: {country: value},
        type: 'post',
        dataType: 'json',
        success: function (response) {
          if(response["currency"]){
            $('#stripe_detail_currency').val(response["currency"])
            return false;
          }
        }
      });
    }else{
      $('#stripe_detail_currency').val("Select One")
    }
  })

  $(".stripe-update-btn").on('click', function(){
    window.location.href = $(this).attr("url")
  })

  $(".stripe-select-btn").on('click', function(){
    var stripe_detail_id = $(this).attr("id")
    var url = $(this).attr("url")
    var selected_id = $("#stripe_loader_"+stripe_detail_id)
    selected_id.show()
    $.ajax({
      url: url,
      data: {stripe_detail_id: stripe_detail_id},
      type: 'post',
      dataType: 'json',
      success: function (response) {
        if(response["success"]){
          selected_id.hide();
          window.location.href = response["path"]
        }else{
          selected_id.hide();
          alert("Something went wrong!")
          return false;
        }
      }
    });

  })

  $(".stripe-delete-btn").on('click', function(){
    var stripe_detail_id = $(this).attr("id")
    var url = $(this).attr("url")
    var not_delete = $(this).attr("not_delete")
    if(not_delete == "true"){
      alert("Please select or add a new account, before deleting the connected account")
      return false;
    }else{
      var x = confirm("Are you sure you want to delete this account?");
      if(x){
          var stripe_detail_id = $(this).attr("id")
          var url = $(this).attr("url")
          var selected_id = $("#stripe_loader_"+stripe_detail_id)
          selected_id.show()
          $.ajax({
            url: url,
            data: {stripe_detail_id: stripe_detail_id},
            type: 'post',
            dataType: 'json',
            success: function (response) {
              if(response["success"]){
                selected_id.hide();
                window.location.href = response["path"]
              }else{
                selected_id.hide();
                alert("Something went wrong!")
                return false;
              }
            }
          });
      }else{
        return false;
      }
    }
  })

  if($("#person_phone_number").length > 0){
    $("#person_phone_number").on("keyup",function(){
      var phone_number = $(this).val()
      var correct_number = $("#correct_number").attr('class')
      if(phone_number.length < 1){
        $("#numeric_number").html("")
        $(".submit-mobile").prop('disabled', false)
      }else{
        if(!$.isNumeric(phone_number)){
          $("#numeric_number").html("<font color='red'>"+correct_number+"<br></font>")
          $(".submit-mobile").prop('disabled', true)
          return false;
        }else{
          $("#numeric_number").html("")
          $(".submit-mobile").prop('disabled', false)
          }
      }
    })
  }

  if($(".submit_card_detail").length > 0){
    $(".submit_card_detail").on('click', function(){
      $(".submit_card_detail").prop('disabled', true)
      $(".card_small_loader").show();
      var url = $("#custom_url").val()
      var name = $("#strip_name").val()
      var number = $("#strip_number").val()
      var cvv = $("#strip_cvv").val()
      var exp_month = $("#strip_exp_month").val()
      var exp_year = $("#strip_exp_year").val()
      name.indexOf(" ") !== -1
      $.trim(name)
      if (name != ""){
        Stripe.setPublishableKey($("#pub_key").val());

        Stripe.card.createToken({
        number: number,
        cvc: cvv,
        exp_month: exp_month,
        exp_year: exp_year
        }, stripeResponseHandler);
      }else{
        $(".strip_error").html("<font style='color:red;'> Invalid Cardholder name </font>")
        $(".submit_card_detail").prop('disabled', false)
        $(".card_small_loader").hide();
        return false;
      }

      function stripeResponseHandler(status, response) {
        if (response.error) {
          $(".submit_card_detail").prop('disabled', false)
          $(".card_small_loader").hide();
          $(".strip_error").html("<font style='color:red;'>"+response.error.message+" </font>")
        } else {
          var transaction_id = $("#transaction_id").val()
          var token = response.id;
          $(".strip_error").html("");
          var fieldsobj = {card_detail: {name: name, number: number, cvv: cvv, exp_month: exp_month, exp_year: exp_year}, token: token, transaction_id: transaction_id}
          $(".submit_card_detail").prop('disabled', true)
          $(".card_small_loader").show();
          $.ajax({
            url: url,
            data: fieldsobj,
            type: 'post',
            dataType: 'json',
            success: function (response) {
              if(response["message"]){
                $(".strip_error").html("<font style='color:red;'>"+response["message"]+" </font>")
                $(".submit_card_detail").prop('disabled', false)
                $(".card_small_loader").hide();
                return false;
              }
              if(response["path"]){
                $(".strip_error").html("");
                $(".submit_card_detail").prop('disabled', false)
                $(".card_small_loader").hide();
                window.location.href = response["path"]
              }
            }
          });

        }
      }

    })
  }
  module.initializePayPalBuyForm = initializePayPalBuyForm;
  module.initializeCreatePaymentPoller = initializeCreatePaymentPoller;
  module.initializeFreeTransactionForm = initializeFreeTransactionForm;

})(window.ST.transaction, _);
