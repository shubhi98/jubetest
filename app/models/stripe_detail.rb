# == Schema Information
#
# Table name: stripe_details
#
#  id                 :integer          not null, primary key
#  country            :string(255)
#  currency           :string(255)
#  routing_number     :string(255)
#  account_number     :string(255)
#  remote_ip          :string(255)
#  dob_day            :integer
#  dob_month          :integer
#  dob_year           :integer
#  city               :string(255)
#  line1              :string(255)
#  postal_code        :string(255)
#  state              :string(255)
#  person_id          :string(255)
#  personal_id_number :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  name               :string(255)
#  status             :boolean
#  account_id         :string(255)
#

class StripeDetail < ActiveRecord::Base
  attr_accessible :name, :account_number, :city, :country, :currency, :dob_day, :dob_month, :dob_year, :line1, :person_id, :personal_id, :personal_id_number, :postal_code, :remote_ip, :routing_number, :state, :status, :account_id

  belongs_to :person
  validates :name, uniqueness: true
  validates_presence_of :name, :account_number, :city, :country, :currency, :dob_day, :dob_month, :dob_year, :line1, :person_id, :personal_id_number, :postal_code, :routing_number, :state

  scope :current_details, -> { where('stripe_details.id is NOT NULL') }

end
