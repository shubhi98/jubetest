class PersonMessagesController < ApplicationController

  before_filter do |controller|
    controller.ensure_logged_in t("layouts.notifications.you_must_log_in_to_send_a_message")
  end

  before_filter :fetch_recipient

  before_filter only: [:new] do |controller|
    controller.ensure_verified "You are not a verified user yet. - Please upload ID under settings, or reach out to us."
  end
  def new
    @conversation = Conversation.new
  end

  def create
    @conversation = new_conversation
    if @conversation.save
      flash[:notice] = t("layouts.notifications.message_sent")
      Delayed::Job.enqueue(MessageSentJob.new(@conversation.messages.last.id, @current_community.id))
      #1 - Each time a user receives a message(contact or request), i will save in the database the date/time of this action
      #2 - When the user responds a message i will also save the date/time information in the database
      #3 - With this saved data, the application will do the math
      #4 - Show the results in the html for each user

      redirect_to @recipient
    else
      flash[:error] = "Sending the message failed. Please try again."
      redirect_to root
    end
  end

  private

  def new_conversation
    conversation = Conversation.new(params[:conversation].merge(community: @current_community))
    conversation.build_starter_participation(@current_user)
    conversation.build_participation(@recipient)
    conversation
  end

  def fetch_recipient
    @recipient = Person.find(params[:person_id])
    if @current_user == @recipient
      flash[:error] = t("layouts.notifications.you_cannot_send_message_to_yourself")
      redirect_to root
    end
  end
end
