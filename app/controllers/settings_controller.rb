class SettingsController < ApplicationController

  before_filter :except => :unsubscribe do |controller|
    controller.ensure_logged_in t("layouts.notifications.you_must_log_in_to_view_your_settings")
  end

  before_filter :only => [:show, :account, :insurance, :id_verification, :strip_update_detail, :stripe_connect, :sms_notification, :notifications] do |controller|
    controller.ensure_authorized_or_admin t("layouts.notifications.you_are_not_authorized_to_view_this_content")
  end

  before_filter :except => [:unsubscribe, :show, :account, :insurance, :id_verification, :strip_update_detail, :stripe_connect, :sms_notification, :notifications] do |controller|
    controller.ensure_authorized t("layouts.notifications.you_are_not_authorized_to_view_this_content")
  end

  def show
    flash.now[:notice] = t("settings.profile.image_is_processing") if @current_user.image.processing?
    @selected_left_navi_link = "profile"
    add_location_to_person
  end

  def account
    @selected_left_navi_link = "account"
    @person.emails.build
    marketplaces = @person.community_memberships
                   .map { |m| Maybe(m.community).name(I18n.locale).or_else(nil) }
                   .compact
    has_unfinished = TransactionService::Transaction.has_unfinished_transactions(@current_user.id)

    render locals: {marketplaces: marketplaces, has_unfinished: has_unfinished}
  end

  def notifications
    @selected_left_navi_link = "notifications"
  end

  def payments
    @selected_left_navi_link = "payments"
  end

  def unsubscribe
    @person_to_unsubscribe = find_person_to_unsubscribe(@current_user, params[:auth])

    if @person_to_unsubscribe && @person_to_unsubscribe.username == params[:person_id] && params[:email_type].present?
      if params[:email_type] == "community_updates"
        MarketplaceService::Person::Command.unsubscribe_person_from_community_updates(@person_to_unsubscribe.id)
      elsif [Person::EMAIL_NOTIFICATION_TYPES, Person::EMAIL_NEWSLETTER_TYPES].flatten.include?(params[:email_type])
        @person_to_unsubscribe.preferences[params[:email_type]] = false
        @person_to_unsubscribe.save!
      else
        @unsubscribe_successful = false
        render :unsubscribe, :status => :bad_request and return
      end
      @unsubscribe_successful = true
      render :unsubscribe
    else
      @unsubscribe_successful = false
      render :unsubscribe, :status => :unauthorized
    end
  end

  def insurance
    @selected_left_navi_link = "insurance"
  end

  def id_verification
    @selected_left_navi_link = "id_verification"
  end

  def update_person_details
    if params[:id_proof]
      @person.update_attributes(:id_proof => params[:id_proof])
      send_email_to_admins
      flash.now[:notice] = "You are uploaded id proof successfully."
      redirect_to person_settings_path(@current_user)
    elsif params[:insurance_papers]
      @person.update_attributes(:insurance_papers => params[:insurance_papers])
      flash.now[:notice] = "You are uploaded insurance papers successfully."
      redirect_to person_settings_path(@current_user)
    else
      flash[:error] = "You are not uploaded file/image successfully."
      render :back
    end
  end

  def strip_credit_detail
    @card_detail= @current_user.build_card_detail
  end

  def create_credit_detail
    @card_detail = @current_user.build_card_detail(params[:card_detail])
    @card_detail.save
    begin
      create_customer(params[:token])
      render :json => { :path => params[:transaction_id].present? ? person_transaction_path(person_id: @current_user.id, id: params[:transaction_id]) : person_settings_path(@current_user) }
    rescue Stripe::CardError => e
      render :json => { :message => e.message}
    end
  end

  def strip_update_detail
    @selected_left_navi_link = "strip_update_detail"
    @card_detail= @person.card_detail
  end

  def update_credit_detail
    @card_detail = @current_user.card_detail(params[:card_detail])
    @card_detail.update_attributes(params[:card_detail])
    begin
      create_customer(params[:token])
      render :json => { :path => params[:transaction_id].present? ? person_transaction_path(person_id: @current_user.id, id: params[:transaction_id]) : person_settings_path(@current_user) }
    rescue Stripe::CardError => e
      render :json => { :message => e.message}
    end
  end

  def sms_notification
    @selected_left_navi_link = "sms_notification"
  end

  def stripe_connect
    @selected_left_navi_link = "stripe_connect"
    @stripe_details = @person.stripe_details.current_details.paginate(:per_page => 5, :page => params[:page])
    if params[:stripe_id].present?
      @stripe_detail = @current_user.stripe_details.find(params[:stripe_id])
    else
      @stripe_detail = @current_user.stripe_details.new
    end
  end

  def get_currency
    country = ISO3166::Country.find_country_by_name(params["country"])
    render :json => { :currency => country.currency.code}
  end

  def save_stripe_details
    params[:stripe_detail][:remote_ip] = request.remote_ip
    @stripe_detail = @current_user.stripe_details.new(params[:stripe_detail])
    if @stripe_detail.valid?
      @stripe_response = @current_user.create_stripe_account(
        params[:stripe_detail], @current_user
      )
      if @stripe_response.class == Array
        render :stripe_connect
      else
        @stripe_detail.account_id = @stripe_response
        @stripe_detail.save
        flash.now[:notice] = "successfully created stripe account."
        redirect_to stripe_connect_person_settings_path(@current_user)
      end
    else
      render :stripe_connect
    end
  end

  def update_stripe_details
    @stripe_detail = @current_user.stripe_details.find(params[:stripe_id])
    if @stripe_detail.update_attributes(params[:stripe_detail])
      @stripe_response = @current_user.update_stripe_account(
        params[:stripe_detail], @current_user, @stripe_detail.account_id
      )
      if @stripe_response.class == Array
        render :stripe_connect
      else
        @stripe_detail.update_attributes(params[:stripe_detail])
        flash.now[:notice] = @stripe_response
        redirect_to stripe_connect_person_settings_path(@current_user)
      end
    else
      render :stripe_connect
    end
  end

  def select_stripe_account
    @stripe_detail = @current_user.stripe_details.find(params[:stripe_detail_id])
    @current_user.stripe_details.update_all(status: nil)
    if @stripe_detail.update_attribute(:status, true)
      @current_user.update_attribute(:strip_account_id, @stripe_detail.account_id)
      render :json => { :success => true, :path => stripe_connect_person_settings_path(@current_user) }
    else
      render :json => {:success => false }
    end
  end

  def delete_stripe_account
    @stripe_detail = @current_user.stripe_details.find(params[:stripe_detail_id])
    Stripe.api_key = STRIPE[:sec_key]
    account = Stripe::Account.retrieve(@stripe_detail.account_id)
    if account.delete
      if @stripe_detail.status
        @current_user.update_attribute(:strip_account_id, nil)
      end
      @stripe_detail.delete
      render :json => { :success => true, :path => stripe_connect_person_settings_path(@current_user) }
    else
      render :json => { :success => false }
    end
  end

  private

  def add_location_to_person
    unless @person.location
      @person.build_location(:address => @person.street_address,:location_type => 'person')
      @person.location.search_and_fill_latlng
    end
  end

  def find_person_to_unsubscribe(current_user, auth_token)
    current_user || Maybe(AuthToken.find_by_token(auth_token)).person.or_else { nil }
  end

  def create_customer(token)
    Stripe.api_key = STRIPE[:sec_key]
    customer = Stripe::Customer.create(card: token,description: "description for #{@current_user.emails.last.address}",email: "#{@current_user.emails.last.address}")
    @current_user.update_attributes(strip_customer_id: customer.id)
  end

  def send_email_to_admins
    admins = @current_community.admins
    admins.each do |admin|
      PersonMailer.send_idupload_notification(admin.emails.first.address, @person, @current_community).deliver
    end
  end

end
