class TransactionsController < ApplicationController
  require 'open-uri'

  before_filter only: [:show] do |controller|
    controller.ensure_logged_in t("layouts.notifications.you_must_log_in_to_view_your_inbox")
  end

  before_filter do |controller|
    controller.ensure_logged_in t("layouts.notifications.you_must_log_in_to_do_a_transaction")
  end

  before_filter only: [:new] do |controller|
    controller.ensure_verified "You are not a verified user yet. - Please upload ID under settings, or reach out to us."
  end

  MessageForm = Form::Message

  TransactionForm = EntityUtils.define_builder(
    [:listing_id, :fixnum, :to_integer, :mandatory],
    [:message, :string],
    [:quantity, :fixnum, :to_integer, default: 1],
    [:start_on, transform_with: ->(v) { Maybe(v).map { |d| TransactionViewUtils.parse_booking_date(d) }.or_else(nil) } ],
    [:end_on, transform_with: ->(v) { Maybe(v).map { |d| TransactionViewUtils.parse_booking_date(d) }.or_else(nil) } ]
  )

  def new
    Result.all(
      ->() {
        fetch_data(params[:listing_id])
      },
      ->((listing_id, listing_model)) {
        ensure_can_start_transactions(listing_model: listing_model, current_user: @current_user, current_community: @current_community)
      }
    ).on_success { |((listing_id, listing_model, author_model, process, gateway))|
      booking = listing_model.unit_type == :day

      transaction_params = HashUtils.symbolize_keys({listing_id: listing_model.id}.merge(params.slice(:start_on, :end_on, :quantity, :delivery)))

      case [process[:process], gateway, booking]
      when matches([:none])
        render_free(listing_model: listing_model, author_model: author_model, community: @current_community, params: transaction_params)
      when matches([:preauthorize, __, true])
        redirect_to book_path(transaction_params)
      when matches([:preauthorize, :paypal])
        redirect_to initiate_order_path(transaction_params)
      when matches([:preauthorize, :braintree])
        redirect_to preauthorize_payment_path(transaction_params)
      when matches([:postpay])
        redirect_to post_pay_listing_path(transaction_params)
      else
        opts = "listing_id: #{listing_id}, payment_gateway: #{gateway}, payment_process: #{process}, booking: #{booking}"
        raise ArgumentError.new("Can not find new transaction path to #{opts}")
      end
    }.on_error { |error_msg, data|
      flash[:error] = Maybe(data)[:error_tr_key].map { |tr_key| t(tr_key) }.or_else("Could not start a transaction, error message: #{error_msg}")
      redirect_to (session[:return_to_content] || root)
    }
  end

  def create
    Result.all(
      ->() {
        TransactionForm.validate(params)
      },
      ->(form) {
        fetch_data(form[:listing_id])
      },
      ->(form, (_, _, _, process)) {
        validate_form(form, process)
      },
      ->(_, (listing_id, listing_model), _) {
        ensure_can_start_transactions(listing_model: listing_model, current_user: @current_user, current_community: @current_community)
      },
      ->(form, (listing_id, listing_model, author_model, process, gateway), _, _) {
        booking_fields = Maybe(form).slice(:start_on, :end_on).select { |booking| booking.values.all? }.or_else({})

        quantity = Maybe(booking_fields).map { |b| DateUtils.duration_days(b[:start_on], b[:end_on]) }.or_else(form[:quantity])

        TransactionService::Transaction.create(
          {
            transaction: {
              community_id: @current_community.id,
              listing_id: listing_id,
              listing_title: listing_model.title,
              starter_id: @current_user.id,
              listing_author_id: author_model.id,
              unit_type: listing_model.unit_type,
              unit_price: Listing.with_discount(listing_model.price, @current_community),
              unit_tr_key: listing_model.unit_tr_key,
              listing_quantity: quantity,
              content: form[:message],
              booking_fields: booking_fields,
              payment_gateway: process[:process] == :none ? :none : gateway, # TODO This is a bit awkward
              payment_process: process[:process]}
          })
      }
    ).on_success { |(_, (_, _, _, process), _, _, tx)|
      person = Person.find(params[:person_id])
      author = Person.find(tx[:transaction][:listing_author_id])
      transaction_id = Transaction.find(tx[:transaction][:id]).id
      person.delay.sms_notify(person, author, transaction_id, "first", params[:locale])
      after_create_actions!(process: process, transaction: tx[:transaction], community_id: @current_community.id)
      flash[:notice] = (after_create_flash(process: process)) # add more params here when needed
      redirect_to after_create_redirect(process: process, starter_id: @current_user.id, transaction: tx[:transaction]) # add more params here when needed
    }.on_error { |error_msg, data|
      flash[:error] = Maybe(data)[:error_tr_key].map { |tr_key| t(tr_key) }.or_else("Could not start a transaction, error message: #{error_msg}")
      redirect_to (session[:return_to_content] || root)
    }
  end

  def show
    m_participant =
      Maybe(
        MarketplaceService::Transaction::Query.transaction_with_conversation(
        transaction_id: params[:id],
        person_id: @current_user.id,
        community_id: @current_community.id))
      .map { |tx_with_conv| [tx_with_conv, :participant] }

    m_admin =
      Maybe(@current_user.has_admin_rights_in?(@current_community))
      .select { |can_show| can_show }
      .map {
        MarketplaceService::Transaction::Query.transaction_with_conversation(
          transaction_id: params[:id],
          community_id: @current_community.id)
      }
      .map { |tx_with_conv| [tx_with_conv, :admin] }

    transaction_conversation, role = m_participant.or_else { m_admin.or_else([]) }

    tx = TransactionService::Transaction.get(community_id: @current_community.id, transaction_id: params[:id])
         .maybe()
         .or_else(nil)

    unless tx.present? && transaction_conversation.present?
      flash[:error] = t("layouts.notifications.you_are_not_authorized_to_view_this_content")
      return redirect_to root
    end

    tx_model = Transaction.where(id: tx[:id]).first
    conversation = transaction_conversation[:conversation]
    listing = Listing.where(id: tx[:listing_id]).first

    messages_and_actions = TransactionViewUtils::merge_messages_and_transitions(
      TransactionViewUtils.conversation_messages(conversation[:messages], @current_community.name_display_type),
      TransactionViewUtils.transition_messages(transaction_conversation, conversation, @current_community.name_display_type))

    MarketplaceService::Transaction::Command.mark_as_seen_by_current(params[:id], @current_user.id)

    is_author =
      if role == :admin
        true
      else
        listing.author_id == @current_user.id
      end

    render "transactions/show", locals: {
      messages: messages_and_actions.reverse,
      transaction: tx,
      listing: listing,
      transaction_model: tx_model,
      conversation_other_party: person_entity_with_url(conversation[:other_person]),
      is_author: is_author,
      role: role,
      message_form: MessageForm.new({sender_id: @current_user.id, conversation_id: conversation[:id]}),
      message_form_action: person_message_messages_path(@current_user, :message_id => conversation[:id]),
      price_break_down_locals: price_break_down_locals(tx)
    }
  end

  def op_status
    process_token = params[:process_token]

    resp = Maybe(process_token)
      .map { |ptok| paypal_process.get_status(ptok) }
      .select(&:success)
      .data
      .or_else(nil)

    if resp
      render :json => resp
    else
      redirect_to error_not_found_path
    end
  end

  def person_entity_with_url(person_entity)
    person_entity.merge({
      url: person_path(id: person_entity[:username]),
      display_name: PersonViewUtils.person_entity_display_name(person_entity, @current_community.name_display_type)})
  end

  def paypal_process
    PaypalService::API::Api.process
  end

  # def auth_stripe_callback
  #   if params["code"].present?
  #     auth_code = RestClient.post 'https://connect.stripe.com/oauth/token', {'client_secret' => STRIPE[:sec_key], 'code' => params["code"], 'grant_type' => 'authorization_code'}.to_json, :content_type => :json, :accept => :json
  #     auth_code_hash = JSON.parse auth_code.gsub('=>', ':')
  #     strip_account_id = auth_code_hash["stripe_user_id"]
  #     @current_user.update_attributes(:strip_account_id => strip_account_id)
  #     redirect_to  stripe_connect_person_settings_path(@current_user)
  #   end
  # end

  def pay_stripe_amount
    @transaction = Transaction.find(params[:id])
    if @transaction.notification_status != "paid"
      Stripe.api_key = STRIPE[:sec_key]
      total_fee = 0
      if params[:convenience_fee].present?
        amount = params[:price].to_f
        convenience_fee = params[:convenience_fee].to_f
        price = amount + convenience_fee
        if @current_community.discount_on_listings == 0
          fee = (amount*(@current_community.application_fee)/100).round(2)
          total_fee = (fee * 100).round
        end
        total_price = (price * 100).round
        total_amount = (amount * 100).round
        rem_price = total_amount - total_fee
        total_fee = total_price - rem_price
      else
        amount = params[:price].to_f
        total_price = (amount * 100).round
        if @current_community.discount_on_listings == 0
          if (@transaction.listing.category_id == 11) || (@transaction.listing.category.url.downcase == "spaces")
            total_fee = (((amount * 5)/100) * 100).round
          else
            total_fee = (((amount * 15)/100) * 100).round
          end
        end
      end
      begin
        charge = Stripe::Charge.create({
            :amount => total_price,
            :currency => @transaction.listing.currency.present? ? @transaction.listing.currency.downcase : "usd",
            :customer => @current_user.strip_customer_id,
            :description => "Payment - jube.co",
            :application_fee => total_fee,
            :destination => @transaction.listing.author.strip_account_id
        })
        if charge.status == "succeeded"
          @transaction.notification_status = "paid"
          @transaction.save
          # #Transfer starts here
          # @transfer  = Stripe::Transfer.create(
          #   {
          #     :amount => total_price,
          #     :currency => "usd",
          #     :recipient => "self",
          #   },
          #   {
          #     :stripe_account => @transaction.listing.author.strip_account_id
          #   }
          # )
          # #end
          receipt_date = Date.strptime(charge.created.to_s, '%s').strftime("%h %d, %Y")
          flash[:notice] = "Payment done successfully!!"
          PersonMailer.delay.inform_renter(@transaction)
          PersonMailer.delay.inform_owner(@transaction)
          app_fee = (amount*(@current_community.application_fee)/100).round(2)
          price = (params[:price].to_f).round(2)
          conv_fee = (params[:convenience_fee].to_f).round(2)
          PersonMailer.delay.mail_owner(@transaction, app_fee, price, receipt_date)
          PersonMailer.delay.mail_renter(@transaction, conv_fee, amount, receipt_date)
          person = @transaction.starter
          author = @transaction.author
          person.delay.sms_notify(person, author, @transaction.id, "confirm renter", params[:locale])
          person.delay.sms_notify(person, author, @transaction.id, "confirm owner", params[:locale])
          @booking = @transaction.booking
          PersonMailer.delay(run_at: (@booking.end_on.to_datetime.end_of_day + 7.hours)).review_mail(@transaction, person)
          PersonMailer.delay(run_at: (@booking.end_on.to_datetime.end_of_day + 7.hours)).review_mail(@transaction, author)
        end
        redirect_to :back
      rescue Stripe::InvalidRequestError => e
        if e.message == "The 'destination' param cannot be set to your own account."
          flash[:error] = "You can not pay to your own account, please choose other account."
        else
          flash[:error] = e.message
        end
        redirect_to person_transaction_path(:person_id => @current_user, :id => @transaction)
      rescue Stripe::CardError => e
        flash[:error] = e.message
        redirect_to person_transaction_path(:person_id => @current_user, :id => @transaction)
      end
    else
      redirect_to person_transaction_path(:person_id => @current_user, :id => @transaction)
    end
  end

  def notification_status
    transaction = Transaction.find(params[:id])
    if transaction.update_attributes(notification_status: params[:notification_status])
      person = transaction.starter
      author = transaction.author
      transaction_id = transaction.id
      person.delay.sms_notify(person, author, transaction.id, params[:notification_status], params[:locale])
      PersonMailer.delay.send_mail_to_renter(transaction)
      PersonMailer.delay.send_mail_to_seller(transaction)
      render json: { status: true, notification_status: params[:notification_status], sender: transaction.starter.full_name, title: transaction.listing.title}
    else
      render json: { status: false }
    end
  end

  private

  def calculate_days(days, a=[])
    num = days
    if num >= 5
      a << 5
      val = num - 5
      calculate_days(val, a)
    elsif num >= 3
      a << 3
      val = num - 3
      calculate_days(val, a)
    elsif num >= 1
      a << 1
      val = num -1
      calculate_days(val, a)
    end
    a
  end

  def ensure_can_start_transactions(listing_model:, current_user:, current_community:)
    error =
      if listing_model.closed?
        "layouts.notifications.you_cannot_reply_to_a_closed_offer"
      elsif listing_model.author == current_user
       "layouts.notifications.you_cannot_send_message_to_yourself"
      elsif !listing_model.visible_to?(current_user, current_community)
        "layouts.notifications.you_are_not_authorized_to_view_this_content"
      else
        nil
      end

    if error
      Result::Error.new(error, {error_tr_key: error})
    else
      Result::Success.new
    end
  end

  def after_create_flash(process:)
    case process[:process]
    when :none
      t("layouts.notifications.message_sent")
    else
      raise NotImplementedError.new("Not implemented for process #{process}")
    end
  end

  def after_create_redirect(process:, starter_id:, transaction:)
    case process[:process]
    when :none
      person_transaction_path(person_id: starter_id, id: transaction[:id])
    else
      raise NotImplementedError.new("Not implemented for process #{process}")
    end
  end

  def after_create_actions!(process:, transaction:, community_id:)
    case process[:process]
    when :none
      # TODO Do I really have to do the state transition here?
      # Shouldn't it be handled by the TransactionService
      MarketplaceService::Transaction::Command.transition_to(transaction[:id], "free")

      # TODO: remove references to transaction model
      transaction = Transaction.find(transaction[:id])

      Delayed::Job.enqueue(MessageSentJob.new(transaction.conversation.messages.last.id, community_id))
      PersonMailer.delay.send_mail_to_renter(transaction)
      PersonMailer.delay.send_mail_to_seller(transaction)
    else
      raise NotImplementedError.new("Not implemented for process #{process}")
    end
  end
  # Fetch all related data based on the listing_id
  #
  # Returns: Result::Success([listing_id, listing_model, author, process, gateway])
  #
  def fetch_data(listing_id)
    Result.all(
      ->() {
        if listing_id.nil?
          Result::Error.new("No listing ID provided")
        else
          Result::Success.new(listing_id)
        end
      },
      ->(listing_id) {
        # TODO Do not use Models directly. The data should come from the APIs
        Maybe(@current_community.listings.where(id: listing_id).first)
          .map     { |listing_model| Result::Success.new(listing_model) }
          .or_else { Result::Error.new("Can not find listing with id #{listing_id}") }
      },
      ->(_, listing_model) {
        # TODO Do not use Models directly. The data should come from the APIs
        Result::Success.new(listing_model.author)
      },
      ->(_, listing_model, *rest) {
        TransactionService::API::Api.processes.get(community_id: @current_community.id, process_id: listing_model.transaction_process_id)
      },
      ->(*) {
        Result::Success.new(MarketplaceService::Community::Query.payment_type(@current_community.id))
      },
    )
  end

  def validate_form(form_params, process)
    if process[:process] == :none && form_params[:message].blank?
      Result::Error.new("Message can not be empty")
    else
      Result::Success.new
    end
  end

  def price_break_down_locals(tx)
    if tx[:payment_process] == :none && tx[:listing_price].cents == 0
      nil
    else
      unit_type = tx[:unit_type].present? ? ListingViewUtils.translate_unit(tx[:unit_type], tx[:unit_tr_key]) : nil
      localized_selector_label = tx[:unit_type].present? ? ListingViewUtils.translate_quantity(tx[:unit_type], tx[:unit_selector_tr_key]) : nil
      booking = !!tx[:booking]
      quantity = tx[:listing_quantity]
      show_subtotal = !!tx[:booking] || quantity.present? && quantity > 1 || tx[:shipping_price].present?
      total_label = (tx[:payment_process] != :preauthorize) ? t("transactions.price") : t("transactions.total")
      listing = Listing.find(tx[:listing_id])
      price_per_day = Listing.with_discount(listing.price, @current_community)
      if booking
        j = []
        (tx[:booking][:start_on]..tx[:booking][:end_on]).group_by(&:wday).values.flatten.each do |i|
          if i.strftime("%a") == "Sat" || i.strftime("%a") == "Sun"
            j << true
          end
        end
        if j.count == 2
          days =  tx[:booking][:duration] - 1
        else
          days =  tx[:booking][:duration]
        end
        if listing.price_3_days && listing.price_5_days
          if days == 3
            list_price = Listing.with_discount(listing.price_3_days.fractional, @current_community)
          elsif days == 4
            list_price = Listing.with_discount(listing.price_3_days.fractional, @current_community) + Listing.with_discount(listing.price.fractional, @current_community)
          elsif days == 5
            list_price = Listing.with_discount(listing.price_5_days.fractional, @current_community)
          elsif days > 5
            list_price = 0
            calculate_days(days).each do |num|
              if num == 3
                list_price += Listing.with_discount(listing.price_3_days.fractional, @current_community)
              elsif num == 5
                list_price += Listing.with_discount(listing.price_5_days.fractional, @current_community)
              else
                list_price += Listing.with_discount(listing.price.fractional, @current_community)
              end
            end
          else
            list_price = Listing.with_discount(listing.price.fractional, @current_community)*days
          end
        else
          list_price = (Listing.with_discount(listing.price.fractional, @current_community))*days
        end
        subtotal = Money.new((list_price), listing.currency)
        service_fee = listing.convenience_fee(subtotal, @current_community)
        total = Money.new((service_fee.fractional + subtotal.fractional), listing.currency)
      else
        days = quantity
      end
      if listing.weekend
        TransactionViewUtils.price_break_down_locals({
          listing_price: tx[:listing_price],
          localized_unit_type: unit_type,
          localized_selector_label: localized_selector_label,
          booking: booking,
          start_on: booking ? tx[:booking][:start_on] : nil,
          end_on: booking ? tx[:booking][:end_on] : tx[:booking][:start_on],
          duration: booking ? days : nil,
          quantity: days,
          subtotal: show_subtotal ? subtotal : nil,
          total: booking ? subtotal : Maybe(tx[:payment_total]).or_else(tx[:checkout_total]),
          shipping_price: tx[:shipping_price],
          total_label: total_label
        })
      else
        TransactionViewUtils.price_break_down_locals({
        listing_price: tx[:listing_price],
        localized_unit_type: unit_type,
        localized_selector_label: localized_selector_label,
        booking: booking,
        start_on: booking ? tx[:booking][:start_on] : nil,
        end_on: booking ? tx[:booking][:end_on] : nil,
        duration: booking ? tx[:booking][:duration] : nil,
        quantity: quantity,
        subtotal: show_subtotal ? tx[:listing_price] * quantity : nil,
        total: Maybe(tx[:payment_total]).or_else(tx[:checkout_total]),
        shipping_price: tx[:shipping_price],
        total_label: total_label
      })
      end
    end
  end

  def render_free(listing_model:, author_model:, community:, params:)
    # TODO This data should come from API
    listing = {
      id: listing_model.id,
      title: listing_model.title,
      action_button_label: t(listing_model.action_button_tr_key),
    }
    author = {
      display_name: PersonViewUtils.person_display_name(author_model, community),
      username: author_model.username
    }

    unit_type = listing_model.unit_type.present? ? ListingViewUtils.translate_unit(listing_model.unit_type, listing_model.unit_tr_key) : nil
    localized_selector_label = listing_model.unit_type.present? ? ListingViewUtils.translate_quantity(listing_model.unit_type, listing_model.unit_selector_tr_key) : nil
    booking_start = Maybe(params)[:start_on].map { |d| TransactionViewUtils.parse_booking_date(d) }.or_else(nil)
    booking_end = Maybe(params)[:end_on].map { |d| TransactionViewUtils.parse_booking_date(d) }.or_else(nil)
    booking = !!(booking_start && booking_end)
    duration = booking ? DateUtils.duration_days(booking_start, booking_end) : nil
    quantity = Maybe(booking ? DateUtils.duration_days(booking_start, booking_end) : TransactionViewUtils.parse_quantity(params[:quantity])).or_else(1)
    total_label = t("transactions.price")
    listing = Listing.find(listing_model.id)
    price_per_day = Listing.with_discount(listing.price, community)
    if booking
      j = []
      (booking_start..booking_end).group_by(&:wday).values.flatten.each do |i|
        if i.strftime("%a") == "Sat" || i.strftime("%a") == "Sun"
          j << true
        end
      end
      if j.count == 2
        days =  duration - 1
      else
        days =  duration
      end
      if listing.price_3_days && listing.price_5_days
        if days == 3
          list_price = Listing.with_discount(listing.price_3_days.fractional, community)
        elsif days == 4
          list_price = Listing.with_discount(listing.price_3_days.fractional, community) + Listing.with_discount(listing.price.fractional, community)
        elsif days == 5
          list_price = Listing.with_discount(listing.price_5_days.fractional, community)
        elsif days > 5
          list_price = 0
          calculate_days(days).each do |num|
            if num == 3
              list_price += Listing.with_discount(listing.price_3_days.fractional, community)
            elsif num == 5
              list_price += Listing.with_discount(listing.price_5_days.fractional, community)
            else
              list_price += Listing.with_discount(listing.price.fractional, community)
            end
          end
        else
          list_price = Listing.with_discount(listing.price.fractional, community)*days
        end
      else
        list_price = (Listing.with_discount(listing.price.fractional, community))*days
      end
      subtotal = Money.new((list_price), listing.currency)
      service_fee = listing.convenience_fee(subtotal, community)
      total = Money.new((service_fee.fractional + subtotal.fractional), listing.currency)
    end

    m_price_break_down = Maybe(listing_model).select { |listing_model| listing_model.price.present? }.map do |listing_model|
      if listing.weekend
        TransactionViewUtils.price_break_down_locals(
          {
            listing_price: Listing.with_discount(listing_model.price, community),
            localized_unit_type: unit_type,
            localized_selector_label: localized_selector_label,
            booking: booking,
            start_on: booking_start,
            end_on: booking_end,
            duration: booking ? days : duration,
            quantity: booking ? days : quantity,
            subtotal: quantity != 1 ? subtotal : nil,
            total: booking ? subtotal : listing_model.price * quantity,
            shipping_price: nil,
            total_label: total_label
          })
      else
        TransactionViewUtils.price_break_down_locals(
          {
            listing_price: Listing.with_discount(listing_model.price, community),
            localized_unit_type: unit_type,
            localized_selector_label: localized_selector_label,
            booking: booking,
            start_on: booking_start,
            end_on: booking_end,
            duration: duration,
            quantity: quantity,
            subtotal: quantity != 1 ? listing_model.price * quantity : nil,
            total: listing_model.price * quantity,
            shipping_price: nil,
            total_label: total_label
          })
      end
    end

    render "transactions/new", locals: {
             listing: listing,
             author: author,
             action_button_label: t(listing_model.action_button_tr_key),
             m_price_break_down: m_price_break_down,
             booking_start: booking_start,
             booking_end: booking_end,
             quantity: quantity,
             form_action: person_transactions_path(person_id: @current_user, listing_id: listing_model.id)
           }
  end



end
