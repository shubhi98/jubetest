class InfosController < ApplicationController

  skip_filter :check_email_confirmation

  def about
    @selected_tribe_navi_tab = "about"
    @selected_left_navi_link = "about"
  end

  def claim
    @selected_tribe_navi_tab = "about"
    @selected_left_navi_link = "claim"
  end

  def terms
    @selected_tribe_navi_tab = "about"
    @selected_left_navi_link = "terms"
  end

  def privacy
    @selected_tribe_navi_tab = "about"
    @selected_left_navi_link = "privacy"
  end

  def trust_and_safety
    @selected_tribe_navi_tab = "about"
    @selected_left_navi_link = "trust_and_safety"
  end

  def faq
    @selected_tribe_navi_tab = "about"
    @selected_left_navi_link = "faq"
  end

  def contact
  end

  private

  def how_to_use_content?
    Maybe(@community_customization).map { |customization| !customization.how_to_use_page_content.nil? }
  end
end
