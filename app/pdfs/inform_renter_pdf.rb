class InformRenterPdf < Prawn::Document
  include MoneyRails::ActionViewExtension
  def initialize(transaction, view)
    super(top_margin: 70)
    @transaction = transaction
    @view = view
    logo
    move_down 20
    booking_details
    move_down 20
    things_to_remember
    move_down 40
    navigation
    move_down 40
    contact_us
  end
  
  def booking_details
    text "The booking of your <b>#{@transaction.listing.title.capitalize}</b> from <b>#{@transaction.author.full_name.capitalize}</b> is completed.", :inline_format => true
    move_down 20
    text "Renting period:  <b>#{@transaction.booking.sat_san} days. #{@transaction.booking.start_on.strftime("%d-%m-%Y")} - #{@transaction.booking.end_on.strftime("%d-%m-%Y")}</b>", :inline_format => true
    text "Remember to coordinate pickup and delivery"
    move_down 20
    text "Rental cost: <b>#{humanized_money_with_symbol(price)}</b>", :inline_format => true
    move_down 10
    text "<b>#{@transaction.author.given_name.capitalize}'s</b> contact Info: <b>#{@transaction.author.full_name.capitalize} - #{@transaction.author.emails.last.address}", :inline_format => true
    text "Address: <b>#{@transaction.author.exact_address.present? ? @transaction.author.exact_address : @transaction.author.street_address}</b>", :inline_format => true
    if @transaction.author.phone_number.present?
      text "Tlf: <b>#{@transaction.author.phone_number}</b>", :inline_format => true
    end
    move_down 20
    if @transaction.author.vat_number.present?
      text "VAT / CVR Number: <b>#{@transaction.author.vat_number}</b>", :inline_format => true
      move_down 20
    end
    text "Payment is now being handled and a receipt will be sent separately to your email, via. Stripe. - It might be necessary to check your spam folder."
  end

  def logo
    logopath =  "#{Rails.root}/app/assets/images/img10.png"
    table([[{image: logopath, position: :center, :image_height => 44, :image_width => 44}]], 
      :row_colors => ["77bbff"], :width => 540,:cell_style => {:border_width => 0})
  end

  def things_to_remember
    image_path = "#{Rails.root}/app/assets/images/img18.png"
    icon = {image: "#{Rails.root}/app/assets/images/img12.png", position: :left, :image_height => 16, :image_width => 16}
    text = make_table([
      ["<b>AS A RENTER, REMEMBER TO:</b>"], 
      [
        make_table([
          [icon, "Coordinate pickup and delivery."],
          [icon, "Treat the equipment like it was your own. "],
          [icon, "You have agreed to follow JUBE’s Terms of Use."],
          [icon, "Reach out to us or the owner, if you have questions about anything."],
          [icon, "Leave a review after delivery"],
          ],:cell_style => {:border_width => 0, :inline_format => true}, :column_widths => [20, 280] 
          )
      ]
      ],:cell_style => {:border_width => 0, :inline_format => true})
    data = [[{image: image_path, position: :left, :image_height => 250, :image_width => 150}, text]]
    table(data,:cell_style => {:border_width => 0, :inline_format => true}, :column_widths => [200, 300] )
  end

  def navigation
    faq_image = {image: "#{Rails.root}/app/assets/images/img13.png", position: :center, :image_height => 64, :image_width => 64}
    inbox_image = {image: "#{Rails.root}/app/assets/images/img14.png", position: :center, :image_height => 64, :image_width => 64}
    terms_image = {image: "#{Rails.root}/app/assets/images/img15.png", position: :center, :image_height => 64, :image_width => 64}
    faq_link = make_cell(:content => "<link href='http://www.jube.co/faq'>Click here for Jube's  FAQ site</link>")
    inbox_link = make_cell(:content => "<link href='https://#{APP_CONFIG.smtp_email_domain}/#{@transaction.starter.username}/inbox'>Click here for your message and booking inbox</link>")
    terms_link = make_cell(:content => "<link href='https://#{APP_CONFIG.smtp_email_domain}/infos/about'>Click here for your message and booking inbox</link>")
    table([
        [faq_image, inbox_image, terms_image],
        ["<b>FAQ</b>", "<b>YOUR INBOX</b>", "<b>TERMS OF USE</b>"],
        [ faq_link, inbox_link, terms_link]
      ],
    :cell_style => {:border_width => 0, :inline_format => true, :align => :center })
  end

  def contact_us
    logo = {image: "#{Rails.root}/app/assets/images/img16.png", position: :left, :image_height => 64, :image_width => 64}
    table([
        [logo, "<b>CONTACT US</b>"],
        [ "Do you have questions, comments, feedback or just want to chat? Reach out to us by email, phone or Intercom (the blue circle in right corner on the website)",
          "JUBE.co<br>Help@jube.co<br>+45 20 51 72 16"
        ]
      ],
    :cell_style => {:border_width => 0, :inline_format => true} )
  end


  def price
    listing = @transaction.listing
    list_price = (listing.price.fractional)*@transaction.booking.sat_san
    @price = Money.new((list_price), listing.currency)
  end
 
end
