class RenterReceiptPdf < Prawn::Document
  include MoneyRails::ActionViewExtension
  def initialize(transaction, fee, price, receipt_date, view)
    super(top_margin: 70)
    @transaction = transaction
    @fee = fee
    @price = price
    @receipt_date = receipt_date
    @total_price = @price + @fee
    @vat_price = (@price * 0.2).round(2)
    logo
    move_down 10
    text "Hallo there! This receipt is sent from JUBE to both parties as a service. - This transaction is made between the Renter and Owner. - The secure online payment is handled in collaboration with Stripe. <b>www.Stripe.com</b> - As an owner the payment usually takes 1-3 business days for to reach your bank account. But in some cases it takes up to 7 business days.- Do you have questions? Reach out to us. ", :inline_format => true
    move_down 10
    owner_renter_details
    receipt_details
    price_details
    move_down 20
    footer
  end

  def logo
    logopath =  "#{Rails.root}/app/assets/images/m_img0.png"
    table([[{image: logopath, position: :left, :image_height => 88, :image_width => 88}, "<b>RECEIPT</b>"],], 
      :width => 540,
      :cell_style => {:border_width => 0, 
                      :valign => :center, 
                      :align => :right,
                      :borders => [:bottom],
                      :border_width => 1,
                      :border_color => "dde5f1",
                      :inline_format => true
                      })
  end

  def owner_renter_details
    renter_img = "#{Rails.root}/app/assets/images/m_img1.png"
    owner_img = "#{Rails.root}/app/assets/images/m_img2.png"
    renter = make_table([["Renter Information:"], ["<b>#{@transaction.starter.full_name.capitalize}</b>"]],:cell_style => {:border_width => 0, :inline_format => true})
    owner = make_table([[" Owner Information:"], ["<b>#{@transaction.author.full_name.capitalize}</b>"]],:cell_style => {:border_width => 0, :inline_format => true})
    renter_info = make_table([
                    [make_table([[{image: renter_img}, renter]],:cell_style => {:border_width => 0, :inline_format => true})],
                    [(@transaction.starter.exact_address.present? ? @transaction.starter.exact_address : @transaction.starter.street_address)],
                    ["Phone: <b>#{@transaction.starter.phone_number}</b> -  VAT/CVR: <b>#{@transaction.starter.vat_number.present? ? @transaction.starter.vat_number : 'Not specified'}</b>"]
                  ],:cell_style => {:border_width => 0, :inline_format => true})
    owner_info = make_table([
                  [make_table([[{image: owner_img}, owner]],:cell_style => {:border_width => 0, :inline_format => true})],
                  [(@transaction.author.exact_address.present? ? @transaction.author.exact_address : @transaction.author.street_address)],
                  ["Phone: <b>#{@transaction.author.phone_number}</b> -  VAT/CVR: <b>#{@transaction.author.vat_number.present? ? @transaction.author.vat_number : 'Not specified'}</b>"]
                ],:cell_style => {:border_width => 0, :inline_format => true})
    table([[renter_info, owner_info]],:width => 540,:cell_style => {:border_width => 1, :border_color => "dde5f1"})
  end

  def receipt_details
    receipt_no_img = "#{Rails.root}/app/assets/images/m_img3.png"
    receipt_date_img = "#{Rails.root}/app/assets/images/m_img4.png"
    receipt_total_img = "#{Rails.root}/app/assets/images/m_img5.png"
    receipt_no = make_table([["Receipt No"], ["<b>#{@transaction.receipt_number}</b>"]],:cell_style => {:border_width => 0, :inline_format => true})
    receipt_date = make_table([["Receipt Date"], ["<b>#{@receipt_date}</b>"]],:cell_style => {:border_width => 0, :inline_format => true})
    receipt_total = make_table([[" Receipt Total"], ["<b>#{@total_price}</b>"]],:cell_style => {:border_width => 0, :inline_format => true})
    table([[
      make_table([[{image: receipt_no_img}, receipt_no]],:cell_style => {:border_width => 0}),
      make_table([[{image: receipt_date_img}, receipt_date]],:cell_style => {:border_width => 0}),
      make_table([[{image: receipt_total_img}, receipt_total]],:cell_style => {:border_width => 0})
      ]],:width => 540,:cell_style => {:border_width => 1, :border_color => "dde5f1"})
  end

  def price_details
    currency = @transaction.unit_price_currency
    table([
      ["<b><color rgb='FFFFFF'>Description</color></b>", "<b><color rgb='FFFFFF'>Fee</color></b>", "<b><color rgb='FFFFFF'>Cost</color></b>", "<b><color rgb='FFFFFF'>Total</color></b>"],
      ["<b>#{@transaction.listing.title.capitalize}</b>", "", "<b><color rgb='67bffd'>#{currency} #{@price}</color></b>", ""],
      ["<b>VAT Accounts For </b>", "", "<b><color rgb='67bffd'>#{currency} #{@vat_price}</color></b>", ""],
      ["<b>Convenience Fee</b>", "<b><color rgb='67bffd'>#{currency} #{@fee}</color></b>", "", ""],
      ["<b>Total Price Transferred</b>", "", "", "<b>#{currency} #{@total_price}</b>"]
      ],:width => 540, :cell_style => {:align => :center, :inline_format => true, :border_width => 1, :border_color => "dde5f1"},
      :row_colors => ["67bffd", "eff3f7", "eff3f7", "eff3f7", "eff3f7"])
  end

  def footer
    table([
      ["All prices include Moms / VAT "],
      ["<b>Website:</b> www.jube.co "],
      ["<b>Address:</b> Bregnerødgade 18, 1 - 2200 Kbh. N. "],
      ["<b>Contact information:</b>  +45 20517216 // +45 53841953 "],
      [" Jube IVS: 38350420 "],
      [""],
      ["<a href='http://www.jube.co'><color rgb='67bffd'>www.jube.co</color></a>"],
      ["<a href='mailto:Help@jube.co'><color rgb='67bffd'>Help@jube.co</color></a>"],
      ["This is a receipt sent from JUBE as a service. If you have any inquiries reach out to us."]
      ],:width => 540, :cell_style => {:align => :center, :inline_format => true, :border_width => 0})
  end
 
end
