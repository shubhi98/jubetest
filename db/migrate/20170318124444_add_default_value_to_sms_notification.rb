class AddDefaultValueToSmsNotification < ActiveRecord::Migration
  def change
    Person.all.each do |i|
      i.sms_notification = true
      i.save
    end
    change_column :people, :sms_notification, :boolean, :default => true
  end
end
