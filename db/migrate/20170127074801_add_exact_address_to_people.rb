class AddExactAddressToPeople < ActiveRecord::Migration
  def change
    add_column :people, :exact_address, :string
  end
end
