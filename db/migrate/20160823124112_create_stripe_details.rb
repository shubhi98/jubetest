class CreateStripeDetails < ActiveRecord::Migration
  def change
    create_table :stripe_details do |t|
      t.string :country
      t.string :currency
      t.string :routing_number
      t.string :account_number
      t.string :remote_ip
      t.integer :dob_day
      t.integer :dob_month
      t.integer :dob_year
      t.integer :personal_id
      t.string :city
      t.string :line1
      t.string :postal_code
      t.string :state
      t.string :person_id
      t.string :personal_id_number

      t.timestamps
    end
  end
end
