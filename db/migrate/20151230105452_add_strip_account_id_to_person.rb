class AddStripAccountIdToPerson < ActiveRecord::Migration
  def change
    add_column :people, :strip_account_id, :string
    add_column :people, :strip_customer_id, :string
  end
end
