class AddSmsNotificationToPerson < ActiveRecord::Migration
  def change
    add_column :people, :sms_notification, :boolean
  end
end
