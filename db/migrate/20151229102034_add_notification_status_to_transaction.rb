class AddNotificationStatusToTransaction < ActiveRecord::Migration
  def change
    add_column :transactions, :notification_status, :string, default: "pending"
  end
end
