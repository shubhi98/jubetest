class AddPricesToListings < ActiveRecord::Migration
  def change
    add_money :listings, :price_3_days, :default => nil
    add_money :listings, :price_5_days, :default => nil
  end
end
