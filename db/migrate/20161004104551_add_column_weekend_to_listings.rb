class AddColumnWeekendToListings < ActiveRecord::Migration
  def change
    add_column :listings, :weekend, :boolean, :default => true
  end
end
