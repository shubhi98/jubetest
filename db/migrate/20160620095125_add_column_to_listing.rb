class AddColumnToListing < ActiveRecord::Migration
  def change
    add_column :listings, :max_replacement_percent, :integer
    add_column :categories, :max_replacement_percent, :text
    add_column :listings, :min_replacement_percent, :integer
    add_column :categories, :min_replacement_percent, :text
  end
end
