class AddColumnsToStripeDetails < ActiveRecord::Migration
  def up
    add_column :stripe_details, :name, :string
    add_column :stripe_details, :status, :boolean
    add_column :stripe_details, :account_id, :string
    remove_column :stripe_details, :personal_id
  end

  def down
    remove_column :stripe_details, :name
    remove_column :stripe_details, :status
    remove_column :stripe_details, :account_id
    add_column :stripe_details, :personal_id, :integer
  end
end
