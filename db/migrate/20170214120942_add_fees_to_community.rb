class AddFeesToCommunity < ActiveRecord::Migration
  def change
    add_column :communities, :application_fee, :integer, :default => 8
    add_column :communities, :service_fee, :integer, :default => 5
  end
end
