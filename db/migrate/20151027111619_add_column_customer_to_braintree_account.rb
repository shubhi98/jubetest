class AddColumnCustomerToBraintreeAccount < ActiveRecord::Migration
  def change
    add_column :braintree_accounts, :customer_id, :integer
    add_column :braintree_accounts, :credit_token, :string
  end
end
