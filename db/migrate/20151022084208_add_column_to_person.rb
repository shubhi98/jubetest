class AddColumnToPerson < ActiveRecord::Migration
  def change
    add_attachment :people, :id_proof
    add_attachment :people, :insurance_papers
    add_column     :people, :is_verify, :boolean, :default => false
  end
end
