class AddCountryCodeToPerson < ActiveRecord::Migration
  def change
    add_column :people, :country_code, :string
  end
end
