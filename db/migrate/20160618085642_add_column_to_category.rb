class AddColumnToCategory < ActiveRecord::Migration
  def change
    add_column :listings, :replacement_percent, :integer
    add_column :categories, :replacement_percent, :text
  end
end
