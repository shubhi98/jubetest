class AddExtraFieldsToCommunityCustomizations < ActiveRecord::Migration
  def change
  	add_column :community_customizations, :trust_and_safety_page_content, :text
  	add_column :community_customizations, :insurance_page_content, :text
  	add_column :community_customizations, :faq_page_content, :text
  end
end
