class AddDiscountOnListingsToCommunities < ActiveRecord::Migration
  def change
    add_column :communities, :discount_on_listings, :integer, :default => 0
  end
end
